<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function requestsPage(){
    	return view('pages.requests');
    }

    public function itemIssuancePage(){
    	return view('pages.itemIssuance');
    }

    public function createRequestPage(){
    	return view('pages.createrequest');
    }

    public function updateRequestPage($id){
    	return view('pages.updateRequest')->with('id', $id);
    }

    public function approvalsPage(){
    	return view('pages.approvals');
    }

    public function userAccountsPage(){
    	return view('pages.userAccounts');
    }

    public function accessMatrixPage(){
    	return view('pages.accessMatrix');
    }
}
